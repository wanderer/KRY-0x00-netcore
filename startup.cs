using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KRY_0x00_netcore
{
    class startup
    {
        static void Main()
        {
            Console.WriteLine($"" +
                "************************************************************\n" +
                "#    # ######  #     #         ###            ###     ###   \n" +
                "#   #  #     #  #   #         #   #  #    #  #   #   #   #  \n" +
                "#  #   #     #   # #         #     #  #  #  #     # #     # \n" +
                "###    ######     #    ##### #     #   ##   #     # #     # \n" +
                "#  #   #   #      #          #     #   ##   #     # #     # \n" +
                "#   #  #    #     #           #   #   #  #   #   #   #   #  \n" +
                "#    # #     #    #            ###   #    #   ###     ###   \n" +
                ".netcore edition\n" +
                "************************************************************\n");
            Console.WriteLine("Welcome to KRY-0x00 .netcore edition");
            Console.WriteLine("This app was created for the purpose of demonstration of affine cipher");
            Console.WriteLine("Press any key to continue...");
            Console.Read();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new main_form());
            Console.Read();
        }
    }
}
