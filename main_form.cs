﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace KRY_0x00_netcore
{
    public partial class main_form : Form
    {
        public main_form()
        {
            InitializeComponent();
        }



        protected bool encr = true;
        protected int enc = 1;
        public char[] alphabet = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
        public char[] allowed_characters = { ' ', '.', ',', '!', '?', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '\n' };
        public char[] getalph()
        {
            return alphabet;
        }

        string user_input;
        public string User_input { get => user_input; set => user_input = richTextBox1.Text; }



        public static string transliterate(string textik)
        {
            if (string.IsNullOrWhiteSpace(textik))
                return textik;

            return MMLib.Extensions.StringExtensions.RemoveDiacritics(textik);
        }

        public string find_bad_boiz(string group_of_mixed_boiz)
        {
            /*
             * allowed chars: "A-Z0123456789 !?.,"
             */
            group_of_mixed_boiz = group_of_mixed_boiz.ToUpper();
            for (int i = 0; i < group_of_mixed_boiz.Length; i++)
            {
                bool report_error = true;

                string a = group_of_mixed_boiz.Substring(i, 1);
                for (int m = 0; m < alphabet.Length; m++)
                {
                    if (a == Convert.ToString(alphabet[m]))
                    {
                        report_error = false;
                    }
                }
                for (int n = 0; n < allowed_characters.Length; n++)
                {
                    if (a == Convert.ToString(allowed_characters[n]))
                    {
                        report_error = false;
                    }
                }

                if (report_error)
                {
                    MessageBox.Show($"error: \"{a}\" is not a valid character", "u screwed");
                }
            }

            if (group_of_mixed_boiz != transliterate(group_of_mixed_boiz))
            {
                _ = MessageBox.Show("diacritics will not be preserved!", "just so you know...", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            group_of_mixed_boiz = transliterate(group_of_mixed_boiz);

            string corrected_group = "";
            for (int i = 0; i < group_of_mixed_boiz.Length; i++)
            {
                string boi = group_of_mixed_boiz.Substring(i, 1);

                /* classic misdirection */
                if (boi == " ")
                { boi = "VYKRICNIKBRO"; }
                else if (boi == "!")
                { boi = "MEDZERABRO"; }
                else if (boi == "?")
                { boi = "BODKABRO"; }
                else if (boi == ".")
                { boi = "OTAZNIKBRO"; }
                else if (boi == ",")
                { boi = "SLONBRO"; }
                else if (boi == "0")
                { boi = "NULABRO"; }
                else if (boi == "1")
                { boi = "JEDNABRO"; }
                else if (boi == "2")
                { boi = "STODESATBRO"; }
                else if (boi == "3")
                { boi = "MILIONBRO"; }
                else if (boi == "4")
                { boi = "PADESATBRO"; }
                else if (boi == "5")
                { boi = "DEVATBRO"; }
                else if (boi == "6")
                { boi = "HNEDBRO"; }
                else if (boi == "7")
                { boi = "CHCEMSPATBRO"; }
                else if (boi == "8")
                { boi = "ORELBRO"; }
                else if (boi == "9")
                { boi = "TRISTOBRO"; }
                else if (boi == "\n")
                { boi = "NEWLINEBRO"; }
                //else
                //{
                /* boi good*/
                //}
                corrected_group = corrected_group + $"{boi}";
            }
            return corrected_group;
        }

        public string reverse_boiz(string group_of_mixed_boiz)
        {
            string corrected_group = "";
            group_of_mixed_boiz = group_of_mixed_boiz.ToUpper();
            group_of_mixed_boiz = group_of_mixed_boiz.Replace(" ", string.Empty);

            for (int i = 0; i < group_of_mixed_boiz.Length; i++)
            {
                Match matchallowed = Regex.Match(group_of_mixed_boiz,
                    @"(VYKRICNIKBRO)|(MEDZERABRO)|(BODKABRO)|(OTAZNIKBRO)|(SLONBRO)|
                        (NULABRO)|(JEDNABRO)|(STODESATBRO)|(MILIONBRO)|(PADESATBRO)|
                        (DEVATBRO)|(HNEDBRO)|(CHCEMSPATBRO)|(ORELBRO)|(TRISTOBRO)|
                        (NEWLINEBRO)", RegexOptions.IgnoreCase);

                if (matchallowed.Success)
                {
                    group_of_mixed_boiz = group_of_mixed_boiz.Replace("VYKRICNIKBRO", " ");
                    group_of_mixed_boiz = group_of_mixed_boiz.Replace("MEDZERABRO", "!");
                    group_of_mixed_boiz = group_of_mixed_boiz.Replace("BODKABRO", "?");
                    group_of_mixed_boiz = group_of_mixed_boiz.Replace("OTAZNIKBRO", ".");
                    group_of_mixed_boiz = group_of_mixed_boiz.Replace("SLONBRO", ",");
                    group_of_mixed_boiz = group_of_mixed_boiz.Replace("NULABRO", "0");
                    group_of_mixed_boiz = group_of_mixed_boiz.Replace("JADNABRO", "1");
                    group_of_mixed_boiz = group_of_mixed_boiz.Replace("STODESATBRO", "2");
                    group_of_mixed_boiz = group_of_mixed_boiz.Replace("MILIONBRO", "3");
                    group_of_mixed_boiz = group_of_mixed_boiz.Replace("PADESATBRO", "4");
                    group_of_mixed_boiz = group_of_mixed_boiz.Replace("DEVATBRO", "5");
                    group_of_mixed_boiz = group_of_mixed_boiz.Replace("HNEDBRO", "6");
                    group_of_mixed_boiz = group_of_mixed_boiz.Replace("CHCEMSPATBRO", "7");
                    group_of_mixed_boiz = group_of_mixed_boiz.Replace("ORELBRO", "8");
                    group_of_mixed_boiz = group_of_mixed_boiz.Replace("TRISTOBRO", "9");
                    group_of_mixed_boiz = group_of_mixed_boiz.Replace("NEWLINEBRO", "\n");
                    continue;
                }
            }
            if (corrected_group == "")
            {
                return group_of_mixed_boiz;
            }
            else return corrected_group;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            /*
             * mode shifting
             */

            enc *= (-1);
            if (enc < 0)
            {
                encr = false;
                button2.Text = "decrypting";
            }
            else if (enc > 0)
            {
                encr = true;
                button2.Text = "encrypting";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /*
             * go button
             * [*] do evil stuff
             */
            bool not_done = true;

            while (not_done)
            {
                // check for empty text - done
                string usr_input = richTextBox1.Text;
                User_input = usr_input;
                if (User_input == "")
                {
                    MessageBox.Show("at least gimme some text", "well...", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    not_done = false;
                    break;
                }


                char[] alph = getalph();
                int kee_a;
                int kee_b;
                try
                {
                    kee_a = Convert.ToInt32(textBox1.Text);
                }
                catch (Exception)
                {

                    _ = MessageBox.Show("attempted to use forbidden value as kee a\nhint - use a number", "repent or die", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;
                }
                try
                {
                    kee_b = Convert.ToInt32(textBox2.Text);
                }
                catch (Exception)
                {

                    _ = MessageBox.Show("attempted to use forbidden value as kee b\nhint - use a number", "repent or die", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;
                }

                do_a_crypto do_a_Crypto = new do_a_crypto();
                richTextBox3.Text = "";
                for (int i = 0; i < alphabet.Length; i++)
                {
                    richTextBox3.Text += alphabet[i];
                }
                richTextBox4.Text = do_a_Crypto.encrypt(richTextBox3.Text, alph, kee_a, kee_b);

                if (encr)
                {
                    Console.WriteLine("user input unparsed: " + User_input);
                    string parsed_input = find_bad_boiz(User_input);
                    Console.WriteLine("user input parsed: " + parsed_input);

                    richTextBox2.Text = do_a_Crypto.encrypt(parsed_input, alph, kee_a, kee_b);
                    Console.WriteLine(richTextBox2.Text);
                    break;
                }
                else if (!encr)
                {
                    richTextBox2.Text = reverse_boiz(do_a_Crypto.decrypt(user_input, alph, kee_a, kee_b));
                    Console.WriteLine(richTextBox2.Text);
                    break;
                }
            }
        }

        private void textBox2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1.PerformClick();
            }
        }
    }
}
